import { Injectable, Logger, HttpStatus, Body, Res } from '@nestjs/common';
import { Transactions, Books } from "./models";
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
const log = require('../Middlewares/logger/requestLogger')
const logger = log()
@Injectable()
export class BooksServices {

    constructor(@InjectModel('Books') private readonly BooksModel: Model<Books>,
        @InjectModel('Transactions') private readonly TransactionModel: Model<Transactions>) {
    }
    async getlist() {
        logger.info('getList is triggered!');
        var axios = require('axios');

        var config = {
            method: 'get',
            url: 'http://127.0.0.1:5001/library-cloud-28ab5/us-central1/getbooks',
            headers: {}
        };

        let result = axios(config)
            .then(function (response) {
                console.log(JSON.stringify(response.data));
                return response.data
            }).catch(function (error) {
                console.log(error);
            });
        return result;

    }
    async addbulkbook(path: String) {
        const Excel = require('exceljs');
        var filename = path
        let workbook = new Excel.Workbook();


        let booksarray: Books[] = await workbook.xlsx.readFile(filename).then(() => {
            var sheet = workbook.getWorksheet("Sheet1");
            console.log(sheet.actualRowCount)
            var books: Books[] = []
            for (var i = 2; i <= sheet.actualRowCount; i++) {
                let data = new Books("", 0, 0, "")
                let obj = []
                for (var j = 1; j <= sheet.actualColumnCount; j++) {
                    obj.push(sheet.getRow(i).getCell(j).toString())
                }

                data.Name = obj[0];
                data.RegId = Number(obj[1])
                data.Count = Number(obj[2])
                data.Author = obj[3]
                books.push(data)
            }
            return books
        })
        console.log(booksarray)
        let count = 0;
        for (let i = 0; i < booksarray.length; i++) {
            let result = await this.addbook(booksarray[i])
            if (result.Code === 201)
                count++;
        }
        return { Code: 201, Count: count, Message: count + "books updated" }


    }
    async addbook(book: Books) {
        logger.info('AddBooks is triggered!');
        let book1 = new this.BooksModel(book)
        let data = await this.BooksModel.find({ RegId: book1.RegId })
        if (!data.length) {
            try {
                let data = await book1.save()
                let result = {
                    Code: 201,
                    Message: "Record Saved !",
                    result: data
                }
                return result
            } catch (error) {
                console.log("Errror in saving Data!", error)
                let result = {
                    Code: 501,
                    Message: "Database errror in saving Data!"
                }
                return result
            }
        } else {
            let result = {
                Code: 401,
                Message: "Book already exist !"
            }
            return result
        }
    }
    async addbookfunction(book: Books) {
        var request = require('request');
        var options = {
            'method': 'POST',
            'url': 'http://127.0.0.1:5001/library-cloud-28ab5/us-central1/app/book/addbooks',
            'headers': {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "Name": book.Name,
                "RegID": book.RegId,
                "Author": book.Author,
                "Count": book.Count
            })

        };
        let result = request(options, function (error, response) {
            if (error) {
                let result = {
                    Code: 500,
                    Message: "Error"
                }
                return result
            };
            console.log(response.body);
            let result = {
                Code: 200,
                Message: "Books added"
            }
            return result
        });

        return result;

    }
    async updatebookissue(REGID: Number) {
        logger.info('UpdateBooksIssue is triggered!');
        let data = await this.BooksModel.find({ RegId: REGID }).lean()
        let id = data[0]._id
        try {
            let data = await this.BooksModel.updateOne({ _id: id, Count: { $gt: 0 } }, { $inc: { Count: -1 } })
            if (!data.modifiedCount) {
                logger.info('Books not available error sent');
                let result = {
                    Code: 501, // CAN SEND 200
                    Message: "Book is not available",
                    resp: data.matchedCount
                }
                return result
            } let result = {
                Code: 201,
                Message: "Record Updated !",
                resp: data.modifiedCount
            }
            return result
        } catch (error) {
            console.log("Errror in updating books count Data!", error)
            logger.info('DataBase Error while updating books count');
            let result = {
                Code: 501,
                Message: "Database errror in updating books Data!"
            }
            return result
        }

    }
    async updatereturnbook(REGID: Number) {
        logger.info(' updatereturnbook is triggered!');
        let data = await this.BooksModel.find({ REGID: REGID })
        let id = data[0]._id
        console.log(id)
        try {
            let data = await this.BooksModel.findByIdAndUpdate({ _id: id }, { $inc: { 'Count': 1 } }, { new: true })
            let result = {
                Code: 201,
                Message: "Book return updated!",
                result: data
            }
            return result
        } catch (error) {
            console.log("Error in increasing books count!", error)
            let result = {
                Code: 501,
                Message: "Database errror in updating data!"
            }
            return result
        }
    }
    async delete(REGID: Number) {
        let data = await this.BooksModel.find({ REGID: REGID })
        let id = data[0]._id
        logger.info(' delete book is triggered!');
        try {
            let resp = await this.BooksModel.findByIdAndDelete(id)

            let result = {
                Code: 201,
                Message: "Book Deleted!",
                result: resp
            }
            return result
        } catch (error) {
            logger.info('DB error in deleting book');
            console.log("DB Error in deleting Book!", error)
            let result = {
                Code: 501,
                Message: "Database errror in deleting data!"
            }
            return result
        }
    }
    async issue(REGID: Number, MIS: Number) {
        logger.info(' issue book fun is triggered!');
        let result = await this.updatebookissue(REGID)
        if (result.Code !== 501 && result.Code !== 200) {
            let resp = await this.addtransaction(MIS, REGID)
            if (resp.Code === 501) {
                return resp
            }
        }
        return result
    }
    async return(REGID: Number, Mis: Number) {
        logger.info(' return book is triggered!');
        let resp = await this.deletetransaction(Mis, REGID)
        if (resp.Code === 501)
            return resp
        let result = await this.updatereturnbook(REGID)
        return result
    }

    async addtransaction(Mis: Number, RegId: Number) {
        logger.info(' add transaction is triggered!');
        let issuedate: Date = new Date()
        let returndate = new Date()
        let transaction = new Transactions(RegId, Mis, issuedate, returndate)
        let record = new this.TransactionModel(transaction)
        try {
            let data = await record.save()
            let result = {
                Code: 201,
                Message: "Transaction record Saved !",
                result: data
            }
            return result
        } catch (error) {
            console.log("Errror in saving Transaction data!", error)
            let result = {
                Code: 501,
                Message: "Database errror in saving transaction!"
            }
            return result
        }
    }
    async deletetransaction(Mis: Number, RegId: Number) {
        logger.info(' delete transaction is triggered!');

        try {
            let resp = await this.TransactionModel.deleteOne({ Mis: Mis })

            let result = {
                Code: 201,
                Message: "Book Transaction Deleted!",
                result: resp
            }
            return result
        } catch (error) {
            console.log("DB Error in deleting Book Transaction!", error)
            let result = {
                Code: 501,
                Message: "Database errror in deleting Transaction data!"
            }
            return result
        }
    }

    async transactiondetails() {
        try {
            let data = await this.TransactionModel.find({})
            let result = {
                Code: 201,
                Message: "Transaction by ID!",
                result: data
            }
            return result
        } catch (error) {
            console.log("Error in searching transactions!", error)
            let result = {
                Code: 501,
                Message: "Error in searching transactions!!"
            }
            return result
        }
    }

    async booksbyid(Mis: Number) {
        logger.info(' Find issued books by ID  is triggered!');

        try {
            let data = await this.TransactionModel.find({ Mis: Mis })
            let result = {
                Code: 201,
                Message: "Book found by ID!",
                result: data
            }
            return result
        } catch (error) {
            console.log("Error in searching books!", error)
            let result = {
                Code: 501,
                Message: "Error in searching books!!"
            }
            return result
        }
    }
}