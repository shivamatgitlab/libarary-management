import { Controller, Get, Post, Patch, Delete, Body, Req,Res, Headers, Param } from '@nestjs/common';
import { Transactions, Books } from './models';
import { BooksServices } from './services';
import { HttpStatus, HttpException } from '@nestjs/common';
import { Response } from 'express';
@Controller('books')
export class BooksControllers {
    constructor(
        private readonly booksservices: BooksServices,
        //private readonly authservice : AuthServices
    ) { }
    // getlist , addbook , updatebook, delete, issue
    @Get('getlist')
    getlist() {
        return this.booksservices.getlist();
    }
    // addbook, getlist, updatebook, deletebook, issuebook, return
    @Patch('issuebook')
    async issuebook(
        @Res() res: Response,
        @Body('RegId') RegId: Number,
        @Body('UserMis') Mis: Number
    ) {
        let result = await this.booksservices.issue(RegId, Mis)
        if (result.Code === 501)
            throw new HttpException('Internal Server Error', HttpStatus.INTERNAL_SERVER_ERROR);

        return res.status(201).send(result.Message)

    }

    @Patch('return')
    async returnbook(
        @Body('RegId') RegId: Number,
        @Body('UserMis') Mis: Number
    ) {
        let result = await this.booksservices.return(RegId, Mis)
        if (result.Code === 501)
            throw new HttpException('Internal Server Error', HttpStatus.INTERNAL_SERVER_ERROR);
        return { StatusCode: result.Code, Message: result.Message }
    }

    @Get('issuedbooks')
    async issuedbooks(
        @Param('Mis') Mis: Number
    ) {
        let result = await this.booksservices.booksbyid(Mis)
        if (result.Code === 501)
            throw new HttpException('Internal Server Error', HttpStatus.INTERNAL_SERVER_ERROR);
        return result
    }

}
@Controller('admin')
export class AdminControllers {
    constructor(
        private readonly booksservices: BooksServices,
        //private readonly authservice : AuthServices
    ) { }
    @Post('addbook')
    async addbook(
        @Body('Name') Name: String,
        @Body('RegId') RegID: Number,
        @Body('Count') count: Number,
        @Body('Author') Author: String
    ) {
        let book1 = new Books(Name, RegID, count, Author)
        let response = await this.booksservices.addbookfunction(book1);
        if (response.Code === 501)
            throw new HttpException('Internal Server Error', HttpStatus.INTERNAL_SERVER_ERROR);
        return { StatusCode: response.Code, Message: response.Message }
    }
    @Post('addbulkbook')
    async adddbulkbook(
        @Body('Path') Path: String
    ) {
        let path = Path
        let response = await this.booksservices.addbulkbook(path)
        return { StatusCode: response.Code, Message: response.Message }

    }
    @Delete('deletebook')
    deletebook(
        @Body('RegId') RegId: Number
    ) {
        this.booksservices.delete(RegId)
    }

    @Patch('issuebook')
    async issuebook(
        @Body('RegId') RegId: Number,
        @Body('UserMis') Mis: Number
    ) {
        let result = await this.booksservices.issue(RegId, Mis)
        return { StatusCode: result.Code, Message: result.Message }

    }

    @Patch('return')
    async returnbook(
        @Body('RegId') RegId: Number,
        @Body('UserMis') Mis: Number
    ) {
        let result = await this.booksservices.return(RegId, Mis)
        return { StatusCode: result.Code, Message: result.Message }
    }


    @Get('transaction')
    transaction() {
        return this.booksservices.transactiondetails()
    }

    @Get('issuedbooks')
    issuedbooks(
        @Param('Mis') Mis: Number
    ) {
        let result = this.booksservices.booksbyid(Mis)
        return result
    }
}

