import { Module, MiddlewareConsumer } from '@nestjs/common';
import { MongooseModule } from "@nestjs/mongoose";
import { BooksControllers, AdminControllers } from "./controllers";
import { BooksSchema } from "./entities/books_schema";
import { BooksServices } from "./services";
import { AuthenticateUser, AuthenticationAdmin } from '../Middlewares/authenticate';
import { TransactionSchema } from './entities/transaction_schema';
@Module({
    imports: [MongooseModule.forFeature([{name: 'Books', schema: BooksSchema},, {name : 'Transactions', schema : TransactionSchema}])],
    controllers: [BooksControllers, AdminControllers],
    providers: [BooksServices],
  })
export class BooksModule{
  configure(consumer: MiddlewareConsumer){
    consumer
      .apply(AuthenticationAdmin)
      .forRoutes('admin/*')
  }
}
