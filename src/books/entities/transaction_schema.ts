import * as mongoose from 'mongoose'
export const TransactionSchema = new mongoose.Schema({
    Mis : { type: Number , required: true },
    RegId: { type: Number, required : true },
    IssueDate : {type : Date , required : true},
    ReturnDate : {type : Date , required : true}
},{ timestamps: true })
export interface Transaction {
    Mis : Number,
    RegId : Number,
    IssueDate : Number,
    ReturnDate: Number

}
