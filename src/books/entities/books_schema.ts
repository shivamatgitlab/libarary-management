import * as mongoose from 'mongoose'
export const BooksSchema = new mongoose.Schema({
    Name : { type: String , required: true },
    RegId : { type: Number , required: true },
    Count: {type: Number , required : true, min : 0, max  : 10},
    Author: { type: String }
},{ timestamps: true })
export interface Books {
    Name :  String
    RegId : Number 
    Count:  Number 
    Author:  String 
}
