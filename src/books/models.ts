export class  Books {
    constructor(
        public Name : String,
        public RegId : Number,
        public Count : Number,
        public Author : String
    ) {
        
    }
}


export class Transactions{
    constructor(
        public RegId : Number,
        public Mis: Number,
        public IssueDate : Date,// use camel case
        public ReturnDate : Date,// usertype add 

    ){

    }
}