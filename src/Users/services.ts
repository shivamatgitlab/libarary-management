import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from 'mongoose';
import { Users, Transaction } from './models';
import { JwtService } from "@nestjs/jwt";

@Injectable()
export class UsersServices {
    constructor(
    @InjectModel('Users') private readonly UsersModel: Model <Users>){
    
    }
    private readonly jwtservice: JwtService = new JwtService
    async getusers(){
        let users = await this.UsersModel.find({})
        console.log(users)
        return users
    }

    async  register(user : Users){
        let userdata = await new this.UsersModel(user)
        let data = await this.UsersModel.find({Mis : userdata.Mis})
        if(! data[0]){
            let result =  await userdata.save()
            let jwt = await this.creatjwt(userdata.Mis, userdata.UserType)
            return { result : result, jwt : jwt}
        }
        else
            return {StatusCode : 201, Message :"User Registered Succesfully !"}
    }

    async creatjwt (Mis : Number, usertype : String){
        const SecretKey = 'Shivam'
        const expiresIn = 60 * 60 * 24;
        const JWT = this.jwtservice.sign({
            Mis : Mis,
            UserType : usertype
          },{expiresIn,secret: SecretKey});
        return JWT
    }

    async login (Mis: Number, Pass : String){
        let data =await this.UsersModel.findOne({Mis : Mis})
        let pass = data.PassWord
        if(pass === Pass){
            let usertype = data.UserType
            return {jwt : await this.creatjwt(Mis, usertype) ,  Status : 201 }
        }
        else{
            return {StatusCode : 419 , Message : " Wrong Credentials!"}
        }
    }
}

