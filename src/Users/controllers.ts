import { Controller, Get, Post, Patch ,Delete, Body, Param } from "@nestjs/common";
import { Request, Response  } from "express"
import { UsersServices } from './services';

@Controller('users')
export class UserControllers{
    constructor(
        private readonly userservices : UsersServices
    ){}
    @Get('getusers')
    getusers(){
        return this.userservices.getusers()
    }
    @Post('register')
    register(
        @Body('Name') Name : String,
        @Body('Mis')Mis : Number,
        @Body('UserType') UserType  :String,
        @Body('PassWord') Password: String
    ){  let user = {
        Mis : Mis,
        Name : Name,
        UserType : UserType,
        PassWord : Password

    }
        let result =  this.userservices.register(user)
        return result

    }
    @Post('login')
    login(
        @Body('Mis')Mis : Number,
        @Body('PassWord')Password : String
    ){
        const result = this.userservices.login(Mis, Password)
        return result
    }
    
}