import { MongooseModule } from "@nestjs/mongoose";
import { MiddlewareConsumer, Module } from "@nestjs/common";
import { UsersServices } from "./services";
import { UserControllers } from './controllers';
import { UsersSchema } from "./entities/users_schema";
import { TransactionSchema } from "./entities/transaction_schema";
import { AuthenticationAdmin } from '../Middlewares/authenticate';
@Module({
    imports: [MongooseModule.forFeature([{name: 'Users', schema:UsersSchema }, {name : 'Transaction', schema : TransactionSchema}])],
    controllers: [UserControllers],
    providers: [UsersServices, AuthenticationAdmin],
  })
export class UserModule{
  configure(consumer: MiddlewareConsumer){
    consumer
      .apply(AuthenticationAdmin)
      .forRoutes( 'users/issuedbooks')
  }
}