import * as mongoose from 'mongoose'
var encrypt = require('mongoose-encryption');

export const UsersSchema = new mongoose.Schema({
    Name : { type: String , required: true },
    Mis : { type: Number , required: true , unique : true},
    UserType: { type: String, required : true },
    PassWord : {type : String , required : true}
},{ timestamps: true })
export interface Users {
    Name :  String
    Mis : Number 
    UserType:  String ,
    PassWord : String
}
var encKey =  require('crypto').randomBytes(32)
var encrypt_token = encKey.toString('base64');
    
var sigKey =  require('crypto').randomBytes(64)
var signKey = sigKey.toString('base64');

console.log(encKey, sigKey)
UsersSchema.plugin(encrypt, { encryptionKey: encrypt_token, signingKey: signKey, encryptedFields: ['PassWord'] });