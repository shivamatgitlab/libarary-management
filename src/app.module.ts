import { Module, NestModule,MiddlewareConsumer } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BooksModule } from './books/modules';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './Users/modules';
import { LoggerMiddleware } from './Middlewares/logger';
import { AuthenticationAdmin, AuthenticateUser } from './Middlewares/authenticate';

@Module({
  imports: [BooksModule,UserModule,
  MongooseModule.forRoot('mongodb+srv://entranceacademyindia:qnbf5DiSKemfRwKj@library.mr77zd5.mongodb.net/?retryWrites=true&w=majority')],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes('*');
    consumer.apply(AuthenticationAdmin).forRoutes('admin/*');
  }
  
}
