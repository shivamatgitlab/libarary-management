import { Injectable, Logger, NestMiddleware, Body } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { Http } from 'winston/lib/winston/transports';
let requestLogger = require('./logger/requestLogger')
@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  logger: any = requestLogger(Http)

  async use(request: Request, response: Response, next: NextFunction){
    const { ip, method, originalUrl } = request;
    const userAgent = request.get('user-agent') || '';
    let data = request.body
    let body = JSON.stringify(data)
    this.logger.http('Method =>'+`${method}`+'    orignalURL =>'+`${originalUrl}`+'   ip =>'+`${ip}` + '   ReqBody =>'+`${body}` + `Res =>`+`${response}`+ 'Res Status =>'+`${response.statusCode}`
    );
    next();
  }
}
