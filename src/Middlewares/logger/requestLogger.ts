import { debug } from "console";
const winston = require('winston')
const { createLogger, format, transports } = require('winston');

const requestLogger = () => {
    return createLogger({
        level: 'debug',
        // format: winston.format.simple(),
        format: format.json(),
    
        defaultMeta: { service: 'user-service' },
        transports: [
            new winston.transports.File({
                filename: 'http.log',
                level : 'http'
                
              }),
            new transports.File({
              filename : 'combined.log',
              level : 'error'
            })
        ],
      });
}

  module.exports = requestLogger ;